feedbackd-device-themes (0.0.r3-1mobian1) unstable; urgency=medium

  * d/gbp.conf: update for mobian
  * d/control: break old device-specific packages.
    Earlier versions of those packages shipped device themes, so they must
    be uninstalled first before installing this package.

 -- Arnaud Ferraris <aferraris@debian.org>  Fri, 03 Feb 2023 11:09:19 +0100

feedbackd-device-themes (0.0.r3-1) unstable; urgency=medium

  * New upstream release
  * Bump standards version

 -- Guido Günther <agx@sigxcpu.org>  Thu, 02 Feb 2023 16:23:15 +0100

feedbackd-device-themes (0.0.r2-1) unstable; urgency=medium

  [ Guido Günther ]
  * New upstream version 0.0.2. We mangle the version number to
    be higher than the date base snapshots. Can be dropped with
    0.1.0.
  * Update upstream metadata
  * d/watch: Use signed tarballs
  * d/gbp.conf: Enable upstream-vcs
  * Enable theme validation
  * Add autopkgtest
  * Bump build and runtime dependencies on feedbackd to 0.0.2 as this version
    is needed to honor the `parent-name` property.

  [ Debian Janitor ]
  * Apply multi-arch hints. + feedbackd-device-themes: Add Multi-Arch:
    foreign.

 -- Guido Günther <agx@sigxcpu.org>  Fri, 16 Dec 2022 09:55:16 +0100

feedbackd-device-themes (0.0.20220523-1) unstable; urgency=medium

  * New upstream version
  * d/copyright: update for new release

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Thu, 26 May 2022 01:12:20 +0200

feedbackd-device-themes (0.0.20210909-2) unstable; urgency=medium

  * Team upload.
  * Source only upload.

 -- Henry-Nicolas Tourneur <debian@nilux.be>  Wed, 22 Dec 2021 16:54:21 +0000

feedbackd-device-themes (0.0.20210909-1) unstable; urgency=medium

  * Initial Debian packaging (Closes: #994180)

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Fri, 10 Sep 2021 10:52:32 +0200
